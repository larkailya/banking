import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseHttpService } from 'src/app/services/base-http.service';
import { Account } from '../models/account.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends BaseHttpService {

  constructor(private readonly http: HttpClient) {
    super();
  }

  getAccounts$(userId: number) {
    return this.http.get<Account[]>(`${this.accountingBaseUrl}/api/v1/users/${userId}/accounts`);
  }
}
