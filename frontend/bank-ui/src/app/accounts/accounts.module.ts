import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsListComponent } from './components/accounts-list/accounts-list.component';
import { AccountsRoutingModule } from './accounts-routing.module';

@NgModule({
  declarations: [
    AccountsListComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule
  ]
})
export class AccountsModule { }
