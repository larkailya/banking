import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subject, catchError, filter, of, switchMap, takeUntil, tap } from 'rxjs';

import { Account } from '../../models/account.model';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit, OnDestroy {
  private readonly _destroyed$ = new Subject<void>();

  isError = false;
  accounts: Account[] = [];

  constructor(
    private readonly accountsService: AccountService,
    private readonly activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this._destroyed$),
        filter((params: Params) => !!params['userId']),
        switchMap((params: Params) => this._getAccounts$(params['userId'])),
        tap((accounts: Account[]) => this.accounts = accounts)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  private _getAccounts$(userId: number): Observable<Account[]> {
    return this.accountsService.getAccounts$(userId)
      .pipe(
        catchError((err) => {
          this.isError = true
          return of(err);
        })
      );
  }

}
