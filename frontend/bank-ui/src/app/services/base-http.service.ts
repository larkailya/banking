import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpService {

  constructor() { }

  get accountingBaseUrl(): string {
    return environment.baseAccountingUrl;
  }

  handleError(error: any) {
    // For now let it be as it is
    console.error(error);
    return error;
  }
}
