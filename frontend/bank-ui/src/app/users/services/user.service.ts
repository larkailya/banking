import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseHttpService } from 'src/app/services/base-http.service';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseHttpService {
  constructor(private http: HttpClient) {
    super();
  }

  getUsers$(): Observable<User[]> {
    return this.http.get<User[]>(`${this.accountingBaseUrl}/api/v1/users`);
  }
}
