export class User {
    firstName!: string;
    lastName!: string;
    id!: number;
    createdDateTime!: Date;
}