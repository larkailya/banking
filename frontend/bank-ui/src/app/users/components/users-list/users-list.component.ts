import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { catchError, finalize, take, tap } from 'rxjs';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  isLoading = true;
  isError = false;
  users: User[] = [];

  constructor(private readonly usersService: UserService) { }

  ngOnInit(): void {
    this.usersService.getUsers$()
      .pipe(
        take(1),
        tap((users: User[]) => this.users = users),
        finalize(() => this.isLoading = false),
        catchError(async () => this.isError = true)
      )
      .subscribe();
  }
}
