import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseHttpService } from 'src/app/services/base-http.service';
import { Transaction } from '../models/transaction.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionService extends BaseHttpService {

  constructor(private readonly http: HttpClient) {
     super();
  }

  getTransactions$(accountId: number) {
    return this.http.get<Transaction[]>(`${this.accountingBaseUrl}/api/v1/accounts/${accountId}/transactions`);
  }
}
