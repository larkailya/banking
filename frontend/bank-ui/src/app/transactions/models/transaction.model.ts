export class Transaction {
    id!: number;
    accountId!: number;
    amount!: number;
    createdDateTime!: Date;
}