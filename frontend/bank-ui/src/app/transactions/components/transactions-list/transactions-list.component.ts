import { Component, OnDestroy, OnInit } from '@angular/core';
import { Transaction } from '../../models/transaction.model';
import { TransactionService } from '../../services/transaction.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subject, catchError, filter, of, switchMap, takeUntil, tap } from 'rxjs';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss']
})
export class TransactionsListComponent implements OnInit,  OnDestroy {

  private readonly _destroyed$ = new Subject<void>();
  isError = false;
  transactions: Transaction[] = [];

  constructor(
    private readonly transactionsService: TransactionService,
    private readonly activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this._destroyed$),
        tap(console.log),
        filter((params: Params) => !!params['accountId']),
        switchMap((params: Params) => this._getTransactions$(params['accountId'])),
        tap((transactions: Transaction[]) => this.transactions = transactions)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  private _getTransactions$(accountId: number): Observable<Transaction[]> {
    return this.transactionsService.getTransactions$(accountId)
    .pipe(catchError((err) => {
      this.isError = true;
      return of(err);
    }));
  }

}
