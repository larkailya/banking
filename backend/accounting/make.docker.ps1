$ErrorActionPreference = 'Stop'

pwsh ./make.ps1
if ($LASTEXITCODE) { exit $LASTEXITCODE }

pwsh ./docker/docker.build.ps1
if ($LASTEXITCODE) { exit $LASTEXITCODE }

try
{
    pwsh ./docker/docker.container.run.ps1
    if ($LASTEXITCODE) { exit $LASTEXITCODE }
}
finally
{
    pwsh ./docker/docker.container.clean.ps1
    if ($LASTEXITCODE) { exit $LASTEXITCODE }

    pwsh ./docker/docker.image.clean.ps1
    if ($LASTEXITCODE) { exit $LASTEXITCODE }
}
