param (
    [string] $ImageRepository = 'services/bank-accounting',
    [int] $Pipeline = 0,
    [string] $Version = '1.0.0.0'
)

$ErrorActionPreference = 'Stop'

if ($Pipeline -ne 0) {
    $tag = "$Version"
} else {
    $tag = 'latest'
}

foreach ($id in docker image ls --quiet --filter reference="${Repository}:$tag" | Get-Unique) {
    docker image rm --force $id
    if (!$?) { exit $LASTEXITCODE }
}

