<#
.Synopsis
        Cleans docker containers.
.DESCRIPTION
        Prerequisites for successful executes is building docker image.
        Script performs runninc docker container with docker image of application.
.EXAMPLE
    pwsh ./docker.conatiner.run.ps1 \
            --Name "<NameOfImage>" \
            --Version "<Version>"
.INPUTS
   Name - The docker image name.
   Version - The docker image version as a tag.
.COMPONENT
    The script belongs to the CI part of the pipeline.
#>
param (
    [string] $Name = 'bank-accounting',
    [string] $Version = '1.0.0.0'
)

$ErrorActionPreference = 'Stop'

foreach ($id in docker container ls --quiet --all --filter name="${Name}.${Version}") {
    docker container stop $id
    if ($LASTEXITCODE) { exit $LASTEXITCODE }

    docker container rm $id
    if ($LASTEXITCODE) { exit $LASTEXITCODE }
}
