<#
.Synopsis
        Cleans docker containers.
.DESCRIPTION
        Prerequisites for successful executes is building docker image.
        Script performs runninc docker container with docker image of application.
.EXAMPLE
    pwsh ./docker.conatiner.run.ps1 \
            --ImageRepository "<YourImageRepository>" \
            --Name "<ImageName>" \
            --Version "<Version>"
.INPUTS
   Name - The docker image name.
   Version - The docker image version as a tag.
.COMPONENT
    The script belongs to the CI part of the pipeline.
#>
param (
    [string] $ImageRepository = 'services',
    [string] $Name = 'bank-accounting',
    [ValidateSet('Production', 'Staging', 'Development')]
    [string] $Environment = 'Development',
    [string] $EnvironmentPrefix = 'Development',
    [int] $PortBase = 55000,
    [string] $Version = '1.0.0.0'
)

$ErrorActionPreference = 'Stop'

if ($Pipeline -ne 0) {
    $tag = "$Version"
} else {
    $tag = 'latest'
}

$ImageName = "${ImageRepository}/${Name}"

docker container run `
    --detach `
    --name "${Name}.${Version}" `
    --env ASPNETCORE_ENVIRONMENT=$Environment `
    --publish ${PortBase}:80 `
    "${ImageName}:${tag}"
if ($LASTEXITCODE) { exit $LASTEXITCODE }

docker container inspect `
    --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' `
		"${Name}.${Version}"
if ($LASTEXITCODE) { exit $LASTEXITCODE }
