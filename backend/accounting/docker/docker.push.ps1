<#
.Synopsis
    Performs pushing built docker images to the image repository.
.DESCRIPTION
    This script performs login to image repository and pushes a built
    docker image to the specified image repository with tag 'latest'
    and passed version.
.EXAMPLE
    pwsh ./docker.push.ps1 \
        -ImageName "<YourImageName>" \
				-BuildNumber "<NumberOfBuild>" \
        -ImageServer "<YourImageServer>" \
        -ImageServerLogin "<YourImageServerLogin>" \
        -ImageServerPassword "<YourImageServerPassword>"
.INPUTS
    ImageName - the name of built docker image (without any tags);
    BuildNumber - the number of build, will appended to release version; 
    ImageServer - the image server link;
    ImageServerLogin - the login to image server with correct rights.
        It could be user login or Service Principle;
    ImageServerPassword - the password for login with correct rights.
        It could be a password of the user credentials or Service Principle.
.COMPONENT
    The script belongs to the CD part of the pipeline.
.FUNCTIONALITY
    Pushing docker images to Image Server.
#>
param (
    [string] $ImageName = "services/bank-accounting",
    [string] $BuildNumber = "0",
    [string] $ImageServer,
    [string] $ImageServerLogin,
    [string] $ImageServerPassword
)

$ErrorActionPreference = "Stop"
$releaseVersion = Get-Content -Path "$PSScriptRoot\..\build\VERSION"
$applicationVersion = "${releaseVersion}.${BuildNumber}"

docker login $ImageServer `
    --username $ImageServerLogin `
    --password $ImageServerPassword
if ($LASTEXITCODE) { exit $LASTEXITCODE }

docker push "${ImageName}:latest"
if ($LASTEXITCODE) { exit $LASTEXITCODE }

docker push "${ImageName}:${applicationVersion}"
if ($LASTEXITCODE) { exit $LASTEXITCODE }
