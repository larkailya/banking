param (
    [Parameter(Mandatory = $true)]
    [string] $Server
)

$ErrorActionPreference = 'Stop'

npx newman run "$PSScriptRoot/postman/collection.json" `
    --folder 'APIs' `
    --globals "$PSScriptRoot/postman/environment.json" `
    --global-var "server=$Server" `
    --reporters cli,json `
    --reporter-json-export "$PSScriptRoot/../testresults/postman.testresults.json"
if ($LASTEXITCODE) { exit $LASTEXITCODE }

