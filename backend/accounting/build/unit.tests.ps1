<#
.Synopsis
		Performs unit test for builded library.
.DESCRIPTION
		Runs unit test and write the result to ./testresult/xunit.testresult.trx file.
.EXAMPLE
		pwsh ./unit.test.ps1
.INPUTS
		No inputs.
.COMPONENT
    The script belongs to the CI part of the pipeline.
#>

dotnet test ./binaries/BankAccounting.Tests.Unit.dll `
  "--logger:trx;LogFileName=$PSScriptRoot/../testresults/xunit.testresults.trx"
if ($LASTEXITCODE) { exit $LASTEXITCODE }
