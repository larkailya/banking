using Bank.Db.Abstractions.Interfaces;
using Bank.Db.Sql.Entities;
using BankAccounting.Contracts.Interfaces;
using BankAccounting.Contracts.Poco;
using BankAccounting.Logic;
using FluentAssertions;
using Moq;

namespace BankAccounting.Tests.Unit.Services;

public class UserServiceTests
{
    private Mock<IRepository<UserDb>> _mockUserRepository = null!;
    private Mock<IRepository<AccountDb>> _mockAccountRepository = null!;
    private Mock<IUnitOfWork> _mockUnitOfWork = null!;
    private IUserService _userService = null!;

    [SetUp]
    public void SetUp()
    {
        _mockUnitOfWork = new Mock<IUnitOfWork>();
        _mockAccountRepository = new Mock<IRepository<AccountDb>>();
        _mockUserRepository = new Mock<IRepository<UserDb>>();
        _userService = new UserService(
            _mockUserRepository.Object,
            _mockAccountRepository.Object,
            _mockUnitOfWork.Object);
    }
    
    [Test]
    public async Task UserService_CreateUserAsync_CallsUserRepositoryAsync()
    {
        // Arrange
        var user = new User
        {
            FirstName = "Joe",
            LastName = "Doe"
        };
        _mockUserRepository.Setup(repository => repository.CreateAsync(It.IsAny<UserDb>(), default));
        
        // Act
        await _userService.CreateUserAsync(user, default);
        
        // Assert
        _mockUserRepository.Verify(
            repository => repository.CreateAsync(It.Is<UserDb>(validation => validation.FirstName == "Joe" && validation.LastName == "Doe"),default),
            Times.Once);
    }
    
    [Test]
    public async Task UserService_OpenAccountForUserAsync_CallsAccountRepositoryAsync()
    {
        // Arrange
        const int userId = 1;
        _mockAccountRepository.Setup(repository => repository.CreateAsync(It.IsAny<AccountDb>(), default));
        
        // Act
        await _userService.OpenAccountForUserAsync(userId, default);
        
        // Assert
        _mockAccountRepository.Verify(
            repository => repository.CreateAsync(It.Is<AccountDb>(validation => validation.UserId == userId && validation.Credit == 0),default),
            Times.Once);
    }
    
    [Test]
    public async Task UserService_GetUserAsync_CallsUserRepositoryAsync()
    {
        // Arrange
        const int userId = 1;
        _mockUserRepository.Setup(
            repository => repository.GetAsync(userId, default))
            .ReturnsAsync(new UserDb
            {
                Id = userId,
                FirstName = "Joe",
                LastName = "Doe"
            });
        
        // Act
        var actualUser = await _userService.GetUserAsync(userId, default);
        
        // Assert
        actualUser.Id.Should().Be(userId);
        actualUser.FirstName.Should().Be("Joe");
        actualUser.LastName.Should().Be("Doe");
    }
}