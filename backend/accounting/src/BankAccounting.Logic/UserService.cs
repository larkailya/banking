﻿using Bank.Db.Abstractions.Interfaces;
using Bank.Db.Sql.Entities;
using BankAccounting.Contracts.Exceptions;
using BankAccounting.Contracts.Interfaces;
using BankAccounting.Contracts.Poco;

namespace BankAccounting.Logic;

/// <summary>
/// Represents a user service containing user related business logic.
/// </summary>
public class UserService : IUserService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IRepository<UserDb> _userRepository;
    private readonly IRepository<AccountDb> _accountRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserService"/> class.
    /// </summary>
    /// <param name="userRepository">Provides methods for accessing users.</param>
    /// <param name="accountRepository">Provides methods for accessing accounts.</param>
    /// <param name="unitOfWork">Provides a way to save changes.</param>
    public UserService(
        IRepository<UserDb> userRepository,
        IRepository<AccountDb> accountRepository,
        IUnitOfWork unitOfWork)
    {
        _userRepository = userRepository;
        _accountRepository = accountRepository;
        _unitOfWork = unitOfWork;
    }

    /// <inheritdoc />
    public async Task CreateUserAsync(User user, CancellationToken cancellationToken)
    {
        var userToCreate = new UserDb
        {
            FirstName = user.FirstName,
            LastName = user.LastName,
            CreatedDateTime = DateTime.UtcNow
        };

        await _userRepository.CreateAsync(userToCreate, cancellationToken);

        await _unitOfWork.CommitAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<User> GetUserAsync(int id, CancellationToken cancellationToken)
    {
        var user = await _userRepository.GetAsync(id, cancellationToken);

        if (user == null)
        {
            throw new NotFoundException();
        }

        return new User
        {
            CreatedDateTime = user.CreatedDateTime,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Id = user.Id
        };
    }

    /// <inheritdoc />
    public async Task<IEnumerable<User>> GetUsersAsync(CancellationToken cancellationToken)
    {
        return (await _userRepository.GetAsync(user => true, cancellationToken)).Select(user => new User
        {
            Id = user.Id,
            FirstName = user.FirstName,
            LastName = user.LastName,
            CreatedDateTime = user.CreatedDateTime 
        });
    }

    /// <inheritdoc />
    public async Task OpenAccountForUserAsync(int userId, CancellationToken cancellationToken)
    {
        var account = new AccountDb
        {
            Credit = 0,
            UserId = userId
        };

        await _accountRepository.CreateAsync(account, cancellationToken);
        
        await _unitOfWork.CommitAsync(cancellationToken);
    }

    /// <inheritdoc />
    public async Task<IEnumerable<Account>> GetUserAccountsAsync(int userId, CancellationToken cancellationToken)
    {
        var accounts = await _accountRepository.GetAsync(account => account.UserId == userId, cancellationToken);

        return accounts.Select(account => new Account
        {
            Id = account.Id,
            Credit = account.Credit
        });
    }
}