using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace BankAccounting.Api.Services.HealthChecks;

internal sealed class DeepHealthCheck : IHealthCheck 
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken)
    {
        return Task.FromResult(HealthCheckResult.Healthy());
    }
}