using BankAccounting.Contracts.Interfaces;

namespace BankAccounting.Api.Services;

internal class DefaultCorrelator : ICorrelator
{
    /// <inheritdoc />
    public string CorrelationId { get; set; }
}