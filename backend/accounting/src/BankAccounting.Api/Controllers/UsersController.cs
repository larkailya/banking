using BankAccounting.Api.Models;
using BankAccounting.Contracts.Interfaces;
using BankAccounting.Contracts.Poco;
using Microsoft.AspNetCore.Mvc;

namespace BankAccounting.Api.Controllers;

/// <summary>
/// Contains a set of APIs for managing users.
/// </summary>
[ApiController]
[Route("api/v1/users")]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;

    /// <summary>
    /// Initializes a new instance of the user controller. 
    /// </summary>
    /// <param name="userService">Provides methods for managing users.</param>
    public UsersController(IUserService userService)
    {
        _userService = userService;
    }

    /// <summary>
    /// Creates a user record without opening an account.
    /// </summary>
    /// <param name="createUserParameters">Provides user parameters.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    [HttpPost]
    public async Task<IActionResult> CreateUserAsync(
        [FromBody] CreateUserParameters createUserParameters,
        CancellationToken cancellationToken)
    {
        await _userService.CreateUserAsync(new User
        {
            FirstName = createUserParameters.FirstName,
            LastName = createUserParameters.LastName,
            CreatedDateTime = DateTime.UtcNow
        }, cancellationToken);

        return Ok();
    }

    /// <summary>
    /// Gets user by identifier.
    /// </summary>
    /// <param name="userId">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    [HttpGet("{userId}")]
    public async Task<IActionResult> GetUserAsync(
        [FromRoute] int userId,
        CancellationToken cancellationToken)
    {
        var user = await _userService.GetUserAsync(userId, cancellationToken);

        return Ok(new UserViewModel
        {
            Id = user.Id,
            FirstName = user.FirstName,
            LastName = user.LastName,
            CreatedDateTime = user.CreatedDateTime
        });
    }
    
    /// <summary>
    /// Gets users.
    /// </summary>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    [HttpGet]
    public async Task<IActionResult> GetUsersAsync(
        CancellationToken cancellationToken)
    {
        return Ok((await _userService.GetUsersAsync(cancellationToken)).Select(user => new UserViewModel
        {
            Id = user.Id,
            FirstName = user.FirstName,
            LastName = user.LastName,
            CreatedDateTime = user.CreatedDateTime 
        }));
    }
}