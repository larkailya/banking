using BankAccounting.Api.Models;
using BankAccounting.Contracts.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BankAccounting.Api.Controllers;

/// <summary>
/// Contains a set of APIs to manage user's accounts.
/// </summary>
[ApiController]
[Route("api/v1/users/{userId}/accounts")]
public sealed class AccountController : ControllerBase
{
    private readonly IUserService _userService;

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountController"/> class.
    /// </summary>
    /// <param name="userService">Provides methods for managing users.</param>
    public AccountController(IUserService userService)
    {
        _userService = userService;
    }

    /// <summary>
    /// Opens an account with zero credit for a user.
    /// </summary>
    /// <param name="userId">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    [HttpPost]
    public async Task<IActionResult> OpenAccountAsync(
        [FromRoute] int userId,
        CancellationToken cancellationToken)
    {
        await _userService.OpenAccountForUserAsync(userId, cancellationToken);

        return Ok();
    }
    
    /// <summary>
    /// Gets user's accounts.
    /// </summary>
    /// <param name="userId">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    [HttpGet]
    public async Task<IActionResult> GetAccountsAsync(
        [FromRoute] int userId,
        CancellationToken cancellationToken)
    {
        var accounts = await _userService.GetUserAccountsAsync(userId, cancellationToken);

        return Ok(accounts.Select(account => new AccountViewModel
        {
            Id = account.Id,
            Credit = account.Credit
        }));
    }
}