namespace BankAccounting.Api.Models;

public sealed class AccountViewModel
{
    public int Id { get; set; }

    public decimal Credit { get; set; }
}