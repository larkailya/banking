namespace BankAccounting.Api.Models;

public sealed class UserViewModel
{
    public int Id { get; set; }
    
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public DateTime CreatedDateTime { get; set; }
}