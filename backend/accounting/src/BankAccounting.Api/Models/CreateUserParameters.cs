using System.ComponentModel.DataAnnotations;

namespace BankAccounting.Api.Models;

public sealed class CreateUserParameters
{
    [Required(AllowEmptyStrings = false)]
    [StringLength(255)]
    public required string FirstName { get; set; }

    [Required(AllowEmptyStrings = false)]
    [StringLength(255)]
    public required string LastName { get; set; }
}