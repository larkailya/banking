// Copyright (C) TripGuider - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential

namespace TripGuider.TripStory.Filters
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Represents the error details.
    /// </summary>
    public sealed class ErrorDetails
    {
        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        ///     Gets or sets the error details.
        /// </summary>
        public IEnumerable<string>? Details { get; set; }

        /// <summary>
        ///     Gets or sets the error trace stack.
        /// </summary>
        public string? Trace { get; set; }

        /// <summary>
        ///     Gets or sets the inner exception.
        /// </summary>
        public Exception? InnerException { get; set; }
    }
}