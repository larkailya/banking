using System.Reflection;

namespace BankAccounting.Api
{
    /// <summary>
    /// Represents information about service.
    /// </summary>
    public static class ApplicationInfo
    {
        static ApplicationInfo()
        {
            var assembly = Assembly.GetEntryAssembly();
            AssemblyName? assemblyName = assembly?.GetName();

            if (assemblyName == null)
            {
                return;
            }

            ApplicationName = assemblyName.Name;
            if (assemblyName.Version is { })
            {
                ApplicationVersion =
                    $"{assemblyName.Version.Major}.{assemblyName.Version.Minor}.{assemblyName.Version.Revision}";
            }
        }

        /// <summary>
        /// Gets application name.
        /// </summary>
        public static string? ApplicationName { get; }

        /// <summary>
        /// Gets application version.
        /// </summary>
        public static string? ApplicationVersion { get; }
    }
}