using Newtonsoft.Json;

namespace BankAccounting.Api.Extensions;

internal static partial class ServiceCollectionExtensions 
{
   
   /// <summary>
   /// Configures service collection for JSON serialization.
   /// </summary>
   /// <param name="serviceCollection">Provides methods for configuring service collection.</param>
   /// <returns>A configured service collection.</returns>
   public static IServiceCollection AddJsonSerializationOptions(this IServiceCollection serviceCollection)
   {
      serviceCollection
         .AddMvc()
         .AddNewtonsoftJson(options =>
         {
            options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            options.SerializerSettings.MaxDepth = 10;
         });

      return serviceCollection;
   } 
}