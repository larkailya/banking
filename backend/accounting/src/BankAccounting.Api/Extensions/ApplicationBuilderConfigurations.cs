using BankAccounting.Api.Middlewares;

namespace BankAccounting.Api.Extensions;

internal static class ApplicationBuilderConfigurations
{
    /// <summary>
    /// Registers middleware that adds correlation id to each request.
    /// </summary>
    /// <param name="app">
    /// The implementation of <see cref="IApplicationBuilder" /> interface.
    /// By default provided from framework infrastructure.
    /// </param>
    /// <returns>The implementation of <see cref="IApplicationBuilder" /> interface.</returns>
    internal static IApplicationBuilder UseCorrelation(this IApplicationBuilder app)
    {
        app.UseMiddleware(typeof(CorrelationMiddleware));

        return app;
    } 
    
    /// <summary>
    /// Registers middleware that adds service information to each request.
    /// </summary>
    /// <param name="app">
    /// The implementation of <see cref="IApplicationBuilder" /> interface.
    /// By default provided from framework infrastructure.
    /// </param>
    /// <returns>The implementation of <see cref="IApplicationBuilder" /> interface.</returns>
    internal static IApplicationBuilder UseServiceInformation(this IApplicationBuilder app)
    {
        app.UseMiddleware(typeof(ServiceInformationMiddleware));

        return app;
    }
}