using Bank.Db.Abstractions.Interfaces;
using Bank.Db.Sql;
using Bank.Db.Sql.Repositories;
using BankAccounting.Api.Extensions;
using BankAccounting.Api.Services;
using BankAccounting.Api.Services.HealthChecks;
using BankAccounting.Contracts.Interfaces;
using BankAccounting.Logic;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace BankAccounting.Api;

public class Startup
{
    private readonly IConfiguration _configuration;
    
    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddCorsPolicy()
            .AddFilters()
            .AddJsonSerializationOptions()
            .AddHealthChecks()
            .AddCheck<ConfigurationHealthCheck>(
                nameof(ConfigurationHealthCheck),
                tags: new[] { nameof(ShallowHealthCheck), nameof(DeepHealthCheck) })
            .AddCheck<ShallowHealthCheck>(nameof(ShallowHealthCheck), tags: new[] { nameof(ShallowHealthCheck) })
            .AddCheck<DeepHealthCheck>(nameof(DeepHealthCheck), tags: new[] { nameof(DeepHealthCheck) })
            .Services
            .AddHttpContextAccessor()
            .AddCorrelation()
            .AddScoped<IUserService, UserService>()
            .AddScoped<ITokenSource, TokenSource>()
            .AddScoped(typeof(IRepository<>), typeof(Repository<>))
            .AddDbContext<BankDbContext>(options =>
            {
                var connectionString = _configuration.GetConnectionString("DatabaseConnection");

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new InvalidOperationException("Database Connection string is not specified.");
                }

                var databaseServerHost = _configuration.GetValue<string>("DBHOST"); 

                options.UseSqlServer(connectionString.Replace("{server}", databaseServerHost), serverOptions => serverOptions.MigrationsAssembly("Bank.Db.Sql"));
            })
            .AddScoped<IUnitOfWork, UnitOfWork>();
    }
    
    /// <summary>
    /// Specifies how the applicationBuilder responds to HTTP request by defining pipeline.
    /// </summary>
    /// <param name="applicationBuilder">Provides methods for configuring host builder.</param>
    /// <param name="hostEnvironment">Provides access to host environment information.</param>
    public void Configure(IApplicationBuilder applicationBuilder, IWebHostEnvironment hostEnvironment)
    {
        applicationBuilder
            .UseCorrelation()
            .UseServiceInformation();

        applicationBuilder.UseStaticFiles();

        if (hostEnvironment.IsDevelopment())
        {
            applicationBuilder.UseDeveloperExceptionPage();
        }

        applicationBuilder
            .UseRequestLocalization()
            .UseHttpsRedirection()
            .UseRouting()
            .UseAuthentication()
            .UseAuthorization()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapHealthChecks(
                    "/health",
                    GetHealthCheckOptions(nameof(ShallowHealthCheck)));
                endpoints.MapHealthChecks(
                    "/health/deep",
                    GetHealthCheckOptions(nameof(DeepHealthCheck)));
            });
    }

    private static HealthCheckOptions GetHealthCheckOptions(string healthCheck)
    {
        var options = new HealthCheckOptions
        {
            Predicate = check => check.Tags.Contains(healthCheck),
            ResultStatusCodes =
            {
                [HealthStatus.Healthy] = StatusCodes.Status200OK,
                [HealthStatus.Unhealthy] = StatusCodes.Status500InternalServerError
            }
        };

        return options;
    }
}