using BankAccounting.Contracts.Interfaces;

namespace BankAccounting.Api.Middlewares
{
    /// <summary>
    /// The middleware that provides request correlation capabilities.
    /// </summary>
    internal class CorrelationMiddleware
    {
        private const string CorrelationIdHeaderName = "X-Correlation-Id";
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrelationMiddleware" /> class.
        /// </summary>
        /// <param name="next">The request delegate to be executed the next.</param>
        public CorrelationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Extracts the correlation identifier from the request if available. Otherwise, generates a new one. The identifier
        ///  is available through the <see cref="ICorrelator" /> interface that could be retrieved using ASP.NET Core dependency
        ///  injection.
        /// </summary>
        /// <param name="context">The HTTP context.</param>
        /// <param name="correlator">The container for keeping the correlation identifier.</param>
        /// <param name="logger">The logging interface.</param>
        /// <returns>The task for asynchronous execution.</returns>
        public Task Invoke(HttpContext context, ICorrelator correlator, ILogger<CorrelationMiddleware> logger)
        {
            var correlationId = context.Request.Headers[CorrelationIdHeaderName].ToString();
            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlator.CorrelationId = correlationId;

            context.Response.OnStarting(
                () =>
                {
                    context.Response.Headers[CorrelationIdHeaderName] = correlationId;

                    return Task.CompletedTask;
                });

            using (logger.BeginScope(new[] { new KeyValuePair<string, object>("CorrelationId", correlationId) }))
            {
                return _next.Invoke(context);
            }
        }
    }
}