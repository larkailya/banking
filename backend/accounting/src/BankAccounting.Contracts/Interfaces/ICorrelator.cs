namespace BankAccounting.Contracts.Interfaces
{
    /// <summary>
    /// Provides access to the correlation identifier.
    /// </summary>
    public interface ICorrelator
    {
        /// <summary>
        /// Gets or sets the correlation identifier.
        /// </summary>
        string CorrelationId { get; set; }
    }
}