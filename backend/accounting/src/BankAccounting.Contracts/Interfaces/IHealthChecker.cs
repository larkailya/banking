namespace BankAccounting.Contracts.Interfaces
{
    /// <summary>
    /// Provides an interface for configuration options checker.
    /// </summary>
    internal interface IHealthChecker
    {
        /// <summary>
        /// Performs checking of configuration options.
        /// </summary>
        void CheckConfigurationOptions();
    }
}