namespace BankAccounting.Contracts.Interfaces
{
    /// <summary>
    /// Provides an interface for service that could perform health check.
    /// </summary>
    public interface IHealthCheckable
    {
        /// <summary>
        /// Performs shallow health check.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous health check.</returns>
        Task RunShallowHealthCheck();

        /// <summary>
        /// Performs deep health check.
        /// </summary>
        /// <returns>A <see cref="Task" /> representing asynchronous health check.</returns>
        Task RunDeepHealthCheck();
    }
}