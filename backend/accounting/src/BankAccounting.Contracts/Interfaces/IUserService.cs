using BankAccounting.Contracts.Poco;

namespace BankAccounting.Contracts.Interfaces;

public interface IUserService
{
    /// <summary>
    /// Creates user.
    /// </summary>
    /// <param name="user">Provides user information.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation with user as a result.</returns>
    Task CreateUserAsync(User user, CancellationToken cancellationToken);

    /// <summary>
    /// Gets user by identifier.
    /// </summary>
    /// <param name="id">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation with user as a result.</returns>
    Task<User> GetUserAsync(int id, CancellationToken cancellationToken);

    /// <summary>
    /// Gets list of users. 
    /// </summary>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation with user as a result.</returns>
    Task<IEnumerable<User>> GetUsersAsync(CancellationToken cancellationToken);

    /// <summary>
    /// Opens a new account for a user with 0 balance.
    /// </summary>
    /// <param name="userId">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    Task OpenAccountForUserAsync(int userId, CancellationToken cancellationToken);

    /// <summary>
    /// Gets all user's accounts. 
    /// </summary>
    /// <param name="userId">Provides user identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    Task<IEnumerable<Account>> GetUserAccountsAsync(int userId, CancellationToken cancellationToken);
}