using System.Runtime.Serialization;

namespace BankAccounting.Contracts.Exceptions
{
    /// <summary>An Exception represents the error resource already exists.</summary>
    [Serializable]
    internal class AlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlreadyExistsException"/> class.
        /// </summary>
        internal AlreadyExistsException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AlreadyExistsException"/> class.
        /// </summary>
        /// <param name="message">The exception descriptive message.</param>
        internal AlreadyExistsException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AlreadyExistsException" /> class.
        /// </summary>
        /// <param name="message">The exception descriptive message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal AlreadyExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AlreadyExistsException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected AlreadyExistsException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}