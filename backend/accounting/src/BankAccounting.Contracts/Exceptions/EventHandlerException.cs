using System.Runtime.Serialization;

namespace BankAccounting.Contracts.Exceptions
{
    /// <summary>
    /// Represents event handler exception.
    /// </summary>
    [Serializable]
    public class EventHandlerException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventHandlerException"/> class.
        /// </summary>
        public EventHandlerException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventHandlerException"/> class.
        /// </summary>
        /// <param name="message">Provides descriptive message.</param>
        public EventHandlerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventHandlerException"/> class.
        /// </summary>
        /// <param name="message">Provides descriptive message.</param>
        /// <param name="inner">Provides inner exception.</param>
        public EventHandlerException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EventHandlerException"/> class.
        /// </summary>
        /// <param name="info">Provides serialization information.</param>
        /// <param name="context">Provides streaming context.</param>
        protected EventHandlerException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}