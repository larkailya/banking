namespace BankAccounting.Contracts.Poco;

public sealed class Account
{
    public int Id { get; set; }
    
    public decimal Credit { get; set; }
}