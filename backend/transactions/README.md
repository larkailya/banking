# BankTransactions.Api 

A rest API services responsible for transactions operations.

# Prerequisites

1. PowerShell
1. Docker
1. .Net Core 7.0
1. Postman
1. newman

## How to run


### Build & Test 

```bash
pwsh ./make.ps1
```

### Build & Test & Run in Docker 

```bash
pwsh ./make.docker.ps1
```

### Gradient Run

There are few scripts which do some work around.
1. `docker/docker.build.ps1` - builds docker image.
2. `docker/docker.container.run.ps1` - runs a built docker image.
3. `docker/docker.image.clean.ps1` - cleans images.
4. `docker/docker.container.clean.ps1` - cleans containers.


> Note: Names for images are unified, if you want to customize them look into script and customize it as you wish.
