<#
.Synopsis
        builind docker image with built application.
.DESCRIPTION
        Prerequisites for successful executes is built application
        to roor/docker/binaries/ as output. 
        Script performs built docker image for application with docker server name.
.EXAMPLE
    pwsh ./docker.build.ps1 \
                    -ImageRepository "<YourImageRepository>" \
                    -BuildNumber "<NumberOfBuild>"
.INPUTS
    ImageRepository - the docker image server.
    BuildNumber - the number of build, will appended to full release version.
.COMPONENT
    The script belongs to the CI part of the pipeline.
#>
param(
    [string] $ImageRepository = "services/bank-transactions",
    [string] $BuildNumber = "0"
)

$ErrorActionPreference = "Stop"

Push-Location -Path "$PSScriptRoot\"

try {

    $releaseVersion = Get-Content -Path "$PSScriptRoot\..\build\VERSION"
    $applicationVersion = "${releaseVersion}.${BuildNumber}"
    $excludedTestsBinaries = @("*.Tests.*");

    Copy-Item -Path "$PSScriptRoot\..\binaries" -Destination . -Recurse -Exclude $excludedTestsBinaries -Force

    docker image build `
        --label "version=${applicationVersion}" `
        --tag "${ImageRepository}:latest" `
        --tag "${ImageRepository}:${applicationVersion}" `
        --file dockerfile ./

    if ($LASTEXITCODE) {
        exit $LASTEXITCODE
    }
}
finally {
    Pop-Location
}
