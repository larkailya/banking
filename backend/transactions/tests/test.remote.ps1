$ErrorActionPreference = 'Stop'

npx newman run "$PSScriptRoot/postman/collection.json" `
    --globals "$PSScriptRoot/postman/environment.json" `
		--global-var "server=" `
    --reporters cli,json `
    --reporter-json-export "$PSScriptRoot/../testresults/postman.testresults.json"
if ($LASTEXITCODE) { exit $LASTEXITCODE }

