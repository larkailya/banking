param (
    [int] $PortBase = 55000,
    [int] $Pipeline = 0
)

$ErrorActionPreference = 'Stop'

& "$PSScriptRoot\test.ps1" -Server "http://localhost:${PortBase}"
if ($LASTEXITCODE) { exit $LASTEXITCODE }

