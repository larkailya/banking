$ErrorActionPreference = 'Stop'

pwsh ./build/build.ps1

if ($LASTEXITCODE) {
	exit $LASTEXITCODE
}

pwsh ./build/unit.tests.ps1

if ($LASTEXITCODE) {
	exit $LASTEXITCODE
}

