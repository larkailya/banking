<#
.Synopsis
    Performs building application using 'dotnet build' command.
.DESCRIPTION
		The script performs building application with no incremental flag and
		release configuration. Gets version of application
		from file VERSION nearby the build script.
.EXAMPLE
		pwsh ./build.ps1
.INPUTS
		No inputs.
.COMPONENT
    The script belongs to the CI part of the pipeline.
#>
$releaseVersion = Get-Content -Path "$PSScriptRoot/VERSION"

dotnet build --no-incremental --configuration Release --output ./binaries `
  /p:Version=$releaseVersion `
  /p:AssemblyVersion=$releaseVersion `
  /p:FileVersion=$releaseVersion `
  /p:DeployedOnBuild=true `
  /p:PublishProfile=FolderProfile `
  /p:UseSharedCompilation=false `
  /nodeReuse:false

if ($LASTEXITCODE) { exit $LASTEXITCODE }
