﻿using Bank.Db.Abstractions.Interfaces;
using Bank.Db.Sql.Entities;
using BankTransactions.Contracts.Exceptions;
using BankTransactions.Contracts.Interfaces;
using BankTransactions.Contracts.Poco;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BankTransactions.Logic;

/// <summary>
/// Represents a transaction service.
/// </summary>
public class TransactionService : ITransactionService
{
    private readonly ILogger<TransactionService> _logger;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IRepository<AccountDb> _accountRepository;
    private readonly IRepository<TransactionDb> _transactionRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="TransactionService"/> class.
    /// </summary>
    /// <param name="accountRepository">Provides methods for accessing accounts.</param>
    /// <param name="transactionRepository">Provides methods for accessing transactions.</param>
    /// <param name="unitOfWork">Provides a way to save changes.</param>
    /// <param name="logger">Provides methods for logging information.</param>
    public TransactionService(
        IRepository<AccountDb> accountRepository,
        IRepository<TransactionDb> transactionRepository,
        IUnitOfWork unitOfWork, ILogger<TransactionService> logger)
    {
        _accountRepository = accountRepository;
        _transactionRepository = transactionRepository;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    /// <inheritdoc />
    public async Task ApplyTransactionAsync(Transaction transaction, CancellationToken cancellationToken)
    {
        var account = await _accountRepository.GetAsync(transaction.AccountId, cancellationToken);

        if (account == null)
        {
            throw new NotFoundException("Account does not exist.");
        }

        account.Credit += transaction.Amount;

        await _transactionRepository.CreateAsync(new TransactionDb
        {
            Amount = transaction.Amount,
            CreatedDateTime = DateTime.UtcNow,
            AccountId = transaction.AccountId
        }, cancellationToken);

        try
        {
            await _accountRepository.UpdateAsync(transaction.AccountId, account, cancellationToken);
        }
        catch (DbUpdateException dbUpdateException)
        {
            _logger.LogWarning(
                exception: dbUpdateException,
                "Concurrency updating occured. Reread data and apply transaction one more time.");
            
            // If this exception occured, we must not commit changes
            return;
        }

        await _unitOfWork.CommitAsync(cancellationToken);
    }

    public async Task<IEnumerable<Transaction>> GetTransactionsAsync(int accountId, CancellationToken cancellationToken)
    {
        return (await _transactionRepository.GetAsync(_ => true, cancellationToken))
            .Select(transaction => new Transaction
            {
                Id = transaction.Id,
                Amount = transaction.Amount,
                AccountId = transaction.AccountId,
                CreatedDateTime = transaction.CreatedDateTime 
            });
    }
}