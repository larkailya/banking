using BankTransactions.Contracts.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TripGuider.TripStory.Filters;

namespace BankTransactions.Api.Filters;

/// <inheritdoc />
/// <summary>
/// Represents the exception filter.
/// </summary>
internal sealed class ExceptionFilter : IExceptionFilter
{
    /// <inheritdoc />
    public void OnException(ExceptionContext context)
    {
        context.Result = context.Exception switch
        {
            BadRequestException badRequestException => new ObjectResult(BuildErrorDetails(badRequestException))
                { StatusCode = StatusCodes.Status400BadRequest },
            AlreadyExistsException _ => new StatusCodeResult(StatusCodes.Status409Conflict),
            ForbiddenException _ => new StatusCodeResult(StatusCodes.Status403Forbidden),
            NotFoundException _ => new StatusCodeResult(StatusCodes.Status404NotFound),
            UnsupportedMediaException _ => new StatusCodeResult(StatusCodes.Status415UnsupportedMediaType),
            UnprocessableException _ => new ObjectResult(BuildErrorDetails(context.Exception))
            {
                StatusCode = StatusCodes.Status422UnprocessableEntity
            },
            UnauthorizedException _ => new StatusCodeResult(StatusCodes.Status401Unauthorized),
            _ => new ObjectResult(BuildErrorDetails(context.Exception))
            {
                StatusCode = StatusCodes.Status500InternalServerError
            }
        };
    }

    private static ErrorDetails BuildErrorDetails(BadRequestException exception)
    {
        return new ErrorDetails
        {
            Code = "Model State Invalid",
            Message = exception.Message,
            Details = exception.ModelState?.SelectMany(state =>
                exception.ModelState[state.Key].Errors
                    ?.Select(error => error.ErrorMessage) ?? Array.Empty<string>())
        };
    }

    private static ErrorDetails BuildErrorDetails(UnprocessableException exception)
    {
        return new ErrorDetails
        {
            Code = "Unprocessable Entity",
            Message = exception.Message
        };
    }

    private static ErrorDetails? BuildErrorDetails(Exception? exception)
    {
        if (exception is null)
        {
            return null;
        }

        var isNotProduction =
            Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != Environments.Production;

        return new ErrorDetails
        {
            Code = exception.GetType()!.FullName!,
            Message = exception.Message,
            Trace = isNotProduction ? exception.StackTrace : null,
            InnerException = isNotProduction ? exception.InnerException : null
        };
    }
}