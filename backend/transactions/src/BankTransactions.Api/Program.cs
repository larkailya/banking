using Serilog;

namespace BankTransactions.Api;

public class Program
{
    /// <summary>
    /// An entry point for application.
    /// </summary>
    /// <param name="args">Run arguments.</param>
    public static void Main(string[] args)
    {
        string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? Environments.Production;

        IConfigurationRoot configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", false, false)
            .AddJsonFile($"appsettings.{env}.json", true, false)
            .AddEnvironmentVariables()
            .Build();

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .Enrich.WithMachineName()
            .Enrich.WithProperty(
                nameof(ApplicationInfo.ApplicationName),
                ApplicationInfo.ApplicationName)
            .Enrich.FromLogContext()
            .CreateLogger();

        try
        {
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseConfiguration(configuration)
                        .UseStartup(typeof(Program).Assembly.FullName!)
                        .UseSerilog();
                })
                .Build()
                .Run();
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Process Terminated.");
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }
}