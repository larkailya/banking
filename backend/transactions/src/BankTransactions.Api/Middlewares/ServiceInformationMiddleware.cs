namespace BankTransactions.Api.Middlewares
{
    /// <summary>
    /// Middleware adds the service information to a request.
    /// </summary>
    public class ServiceInformationMiddleware
    {
        private const string ApplicationNameHeaderName = "X-Service-Name";
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceInformationMiddleware" /> class.
        /// </summary>
        /// <param name="next">The instance of <see cref="RequestDelegate" /> class.</param>
        public ServiceInformationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Request handling method.
        /// </summary>
        /// <param name="httpContext">The HTTP context.</param>
        /// <returns>A <see cref="Task" /> representing the result of the asynchronous operation.</returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            httpContext.Response.Headers.Add(ApplicationNameHeaderName, ApplicationInfo.ApplicationName);
            await _next(httpContext);
        }
    }
}