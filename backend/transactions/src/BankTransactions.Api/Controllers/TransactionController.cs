using BankTransactions.Contracts.Interfaces;
using BankTransactions.Api.Models;
using BankTransactions.Contracts.Poco;
using Microsoft.AspNetCore.Mvc;

namespace BankTransactions.Api.Controllers;

[ApiController]
[Route("api/v1/accounts/{accountId}/transactions")]
public sealed class TransactionController : ControllerBase
{
    private readonly ITransactionService _transactionService;

    public TransactionController(ITransactionService transactionService)
    {
        _transactionService = transactionService;
    }

    [HttpPost]
    public async Task<IActionResult> ApplyTransactionAsync(
        [FromRoute] int accountId,
        [FromBody] TransactionApplicationParameters transactionApplicationParameters,
        CancellationToken cancellationToken)
    {
        await _transactionService.ApplyTransactionAsync(new Transaction
            {
                Amount = transactionApplicationParameters.Amount,
                AccountId = accountId
            },
            cancellationToken);

        return Ok();
    }

    [HttpGet]
    public async Task<IActionResult> GetTransactionsAsync(int accountId, CancellationToken cancellationToken)
    {
        return Ok(await _transactionService.GetTransactionsAsync(accountId, cancellationToken));
    }
}