namespace BankTransactions.Api.Models;

public sealed class TransactionViewModel 
{
    public int Id { get; set; }
    
    public int AccountId { get; set; }

    public decimal Amount { get; set; }

    public DateTime CreatedDateTime { get; set; }
}