using System.ComponentModel.DataAnnotations;

namespace BankTransactions.Api.Models;

public sealed class TransactionApplicationParameters 
{
    [Required]
    [Range(0.0, double.PositiveInfinity)]
    public decimal Amount  { get; set; }
}