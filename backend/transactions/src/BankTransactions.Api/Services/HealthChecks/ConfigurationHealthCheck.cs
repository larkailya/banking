using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace BankTransactions.Api.Services.HealthChecks;

internal sealed class ConfigurationHealthCheck : IHealthCheck 
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
    {
        return Task.FromResult(HealthCheckResult.Healthy());
    }
}