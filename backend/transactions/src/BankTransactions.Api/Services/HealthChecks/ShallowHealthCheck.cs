using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace BankTransactions.Api.Services.HealthChecks;

internal sealed class ShallowHealthCheck : IHealthCheck 
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken)
    {
        return Task.FromResult(HealthCheckResult.Healthy());
    }
}