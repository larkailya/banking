using BankTransactions.Contracts.Interfaces;

namespace BankTransactions.Api.Services;

internal class DefaultCorrelator : ICorrelator
{
    /// <inheritdoc />
    public string CorrelationId { get; set; }
}