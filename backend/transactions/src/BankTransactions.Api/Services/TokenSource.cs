using BankTransactions.Contracts.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;

namespace BankTransactions.Api.Services;

internal sealed class TokenSource : ITokenSource
{
    private readonly IHttpContextAccessor _accessor;

    /// <summary>
    /// Initializes a new instance of the <see cref="TokenSource"/> class.
    /// </summary>
    /// <param name="accessor">The implementation of <see cref="IHttpContextAccessor" /> interface.</param>
    public TokenSource(IHttpContextAccessor accessor)
    {
        _accessor = accessor;
    }

    /// <inheritdoc />
    public string? JwtToken
    {
        get
        {
            if (_accessor.HttpContext!.Request.Headers.TryGetValue(
                    HeaderNames.Authorization,
                    out StringValues values))
            {
                return values
                    .ToArray()
                    ?.Select(ParseJwtToken)
                    .FirstOrDefault(value => value != null);
            }

            return null;
        }
    }

    private static string? ParseJwtToken(string? value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return null;
        }

        string?[] parts = value.Split(" ", StringSplitOptions.RemoveEmptyEntries);

        if (parts.Length != 2 ||
            string.CompareOrdinal(parts[0], JwtBearerDefaults.AuthenticationScheme) != 0)
        {
            return null;
        }

        return parts[1];
    }  
}