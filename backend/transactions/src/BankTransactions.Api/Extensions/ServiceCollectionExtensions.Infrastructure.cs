using BankTransactions.Contracts.Interfaces;
using BankTransactions.Api.Filters;
using BankTransactions.Api.Services;

namespace BankTransactions.Api.Extensions;

internal static partial class ServiceCollectionExtensions 
{
    /// <summary>
    ///     Configures a CORS policy.
    /// </summary>
    /// <param name="services">Provides methods for configuring service collection.</param>
    /// <returns>Configured implementation of <see cref="IServiceCollection" /> interface.</returns>
    internal static IServiceCollection AddCorsPolicy(this IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy(
                "CorsPolicy",
                builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
        });

        return services;
    }

    /// <summary>
    ///     Adds filters.
    /// </summary>
    /// <param name="services">
    ///     The implementation of <see cref="IServiceCollection" /> interface.
    ///     By default provided from framework infrastructure.
    /// </param>
    /// <returns>The implementation of <see cref="IServiceCollection" /> interface.</returns>
    internal static IServiceCollection AddFilters(
        this IServiceCollection services)
    {
        services
            .AddControllers(options => { options.Filters.Add(typeof(ExceptionFilter)); });

        return services;
    }

    /// <summary>
    ///     Extends the application services with the correlation services.
    /// </summary>
    /// <param name="services">The collection of services to configure the application with.</param>
    /// <returns>The updated collection of services to configure the application with.</returns>
    internal static IServiceCollection AddCorrelation(this IServiceCollection services)
    {
        return services.AddScoped<ICorrelator, DefaultCorrelator>();
    }
}