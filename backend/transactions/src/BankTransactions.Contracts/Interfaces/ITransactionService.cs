using BankTransactions.Contracts.Poco;

namespace BankTransactions.Contracts.Interfaces;

public interface ITransactionService
{
    /// <summary>
    /// Applies transaction.
    /// </summary>
    /// <param name="transaction">Provides transaction information.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation with user as a result.</returns>
    Task ApplyTransactionAsync(Transaction transaction, CancellationToken cancellationToken);

    /// <summary>
    /// Gets a list of transactions for account.
    /// </summary>
    /// <param name="accountId">Provides account identifier.</param>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation with user as a result.</returns>
    Task<IEnumerable<Transaction>> GetTransactionsAsync(int accountId, CancellationToken cancellationToken);
}