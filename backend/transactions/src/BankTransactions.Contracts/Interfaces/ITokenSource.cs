namespace BankTransactions.Contracts.Interfaces
{
    /// <summary>
    /// Provides an interface for token source provider.
    /// </summary>
    public interface ITokenSource
    {
        /// <summary>
        /// Gets the token from HTTP request.
        /// </summary>
        public string JwtToken { get; }
    }
}