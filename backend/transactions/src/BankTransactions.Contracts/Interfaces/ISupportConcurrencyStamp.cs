namespace BankTransactions.Contracts.Interfaces;

public interface ISupportConcurrencyStamp
{
    public byte[] ConcurrencyToken { get; set; } 
}