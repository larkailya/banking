namespace BankTransactions.Contracts.Poco;

public sealed class Account
{
    public int Id { get; set; }
    
    public decimal Credit { get; set; }

    public ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}