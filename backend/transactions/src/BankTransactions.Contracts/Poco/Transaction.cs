namespace BankTransactions.Contracts.Poco;

public sealed class Transaction 
{
    public int Id { get; set; }
    
    public decimal Amount { get; set; }
    
    public int AccountId { get; set; }
    
    public DateTime CreatedDateTime { get; set; }
}