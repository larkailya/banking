using System.Runtime.Serialization;

namespace BankTransactions.Contracts.Exceptions
{
    /// <summary>
    /// An Exceptions represents unsupported media HTTP status.
    /// </summary>
    [Serializable]
    internal class UnsupportedMediaException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnsupportedMediaException" /> class.
        /// </summary>
        internal UnsupportedMediaException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnsupportedMediaException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        internal UnsupportedMediaException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///  Initializes a new instance of the <see cref="UnsupportedMediaException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal UnsupportedMediaException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="UnsupportedMediaException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected UnsupportedMediaException(SerializationInfo serializationInfo, StreamingContext streamingContext)
        {
        }
    }
}