using System.Runtime.Serialization;

namespace BankTransactions.Contracts.Exceptions
{
    /// <summary>
    /// An Exception represents the forbidden HTTP status.
    /// </summary>
    [Serializable]
    internal class ForbiddenException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForbiddenException" /> class.
        /// </summary>
        internal ForbiddenException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ForbiddenException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        internal ForbiddenException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ForbiddenException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal ForbiddenException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        private ForbiddenException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}