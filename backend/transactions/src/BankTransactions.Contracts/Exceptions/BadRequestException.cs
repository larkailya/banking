using System.Runtime.Serialization;
using System.Web.Http.ModelBinding;

namespace BankTransactions.Contracts.Exceptions
{
    /// <summary>
    /// An exception represents the bad request HTTP status.
    /// </summary>
    [Serializable]
    internal class BadRequestException : Exception
    {
        private const string BadRequestMessage = "Bad Request Model State";

        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException"/> class.
        /// </summary>
        internal BadRequestException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException"/> class.
        /// </summary>
        /// <param name="message">The bad request descriptive error message.</param>
        internal BadRequestException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        /// <param name="innerException">The inner exception.</param>
        internal BadRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException"/> class.
        /// </summary>
        /// <param name="modelState">The model state dictionary.</param>
        internal BadRequestException(ModelStateDictionary? modelState)
            : base(BadRequestMessage)
        {
            ModelState = modelState;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadRequestException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization information.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected BadRequestException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
            ModelState =
                (ModelStateDictionary)serializationInfo.GetValue(nameof(ModelState), typeof(ModelStateDictionary))!;
        }

        /// <summary>
        /// Gets or sets the model state dictionary.
        /// </summary>
        public ModelStateDictionary? ModelState { get; set; }

        /// <inheritdoc />
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            info.AddValue(nameof(ModelState), ModelState);

            base.GetObjectData(info, context);
        }
    }
}