using System.Runtime.Serialization;

namespace BankTransactions.Contracts.Exceptions
{
    /// <summary>
    /// Represents the exception when entity is not valid for processing.
    /// </summary>
    [Serializable]
    public class UnprocessableException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnprocessableException" /> class.
        /// </summary>
        public UnprocessableException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnprocessableException" /> class.
        /// </summary>
        /// <param name="message">The description exception message.</param>
        public UnprocessableException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnprocessableException" /> class.
        /// </summary>
        /// <param name="message">The descriptive exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public UnprocessableException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnprocessableException" /> class.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected UnprocessableException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}