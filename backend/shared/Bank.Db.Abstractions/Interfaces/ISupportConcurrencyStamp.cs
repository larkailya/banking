namespace Bank.Db.Abstractions.Interfaces;

/// <summary>
/// Provides unified interface for entities which support concurrency handling.
/// </summary>
public interface ISupportConcurrencyStamp
{
    /// <summary>
    /// Gets or sets concurrency token.
    /// </summary>
    public byte[] ConcurrencyToken { get; set; } 
}