using System.Linq.Expressions;

namespace Bank.Db.Abstractions.Interfaces;

/// <summary>
/// Provides an interface for persistent storage.
/// </summary>
public interface IRepository<TEntity> where TEntity : class
{
    Task CreateAsync(TEntity entity, CancellationToken cancellationToken);

    Task UpdateAsync(int id, TEntity entity, CancellationToken cancellationToken);

    Task<TEntity?> GetAsync(int id, CancellationToken cancellationToken);

    Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken);
}