namespace Bank.Db.Abstractions.Interfaces;

/// <summary>
/// Provides interface for enterprise pattern providing transactions functionality.
/// </summary>
public interface IUnitOfWork
{
    /// <summary>
    /// Commits changes to database.
    /// </summary>
    /// <param name="cancellationToken">Provides a way to cancel asynchronous operation.</param>
    /// <returns>A <see cref="Task"/> representing asynchronous operation.</returns>
    Task CommitAsync(CancellationToken cancellationToken);
}