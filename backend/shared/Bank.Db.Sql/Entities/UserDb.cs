using System.ComponentModel.DataAnnotations;
using Bank.Db.Abstractions.Interfaces;

namespace Bank.Db.Sql.Entities;

public sealed class UserDb : ISupportConcurrencyStamp
{
    public int Id { get; set; }

    public required string FirstName { get; set; }

    public required string LastName { get; set; }

    public ICollection<AccountDb> Accounts { get; set; } = new List<AccountDb>();

    [Timestamp]
    public byte[] ConcurrencyToken { get; set; } = null!;

    public DateTime CreatedDateTime { get; set; }
}