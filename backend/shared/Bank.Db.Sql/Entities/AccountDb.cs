using System.ComponentModel.DataAnnotations;
using Bank.Db.Abstractions.Interfaces;

namespace Bank.Db.Sql.Entities;

public sealed class AccountDb : ISupportConcurrencyStamp
{
    public int Id { get; set; }

    public decimal Credit { get; set; }

    public int UserId { get; set; }

    [Timestamp]
    public byte[] ConcurrencyToken { get; set; } = null!;

    public UserDb User { get; set; } = null!;

    public ICollection<TransactionDb> Transactions { get; set; } = new List<TransactionDb>();
}