namespace Bank.Db.Sql.Entities;

public sealed class TransactionDb
{
    public int Id { get; set; }

    public decimal Amount { get; set; }

    public DateTime CreatedDateTime { get; set; }

    public int AccountId { get; set; }
    
    public AccountDb Account { get; set; } = null!;
}