﻿using Bank.Db.Sql.Entities;
using Bank.Db.Sql.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Bank.Db.Sql;

/// <summary>
/// Represents database context.
/// </summary>
public sealed class BankDbContext : DbContext
{
    /// <inheritdoc />
    public BankDbContext(DbContextOptions<BankDbContext> dbContextOptions) : base(dbContextOptions)
    {
    }
    
    /// <summary>
    /// Gets or sets accounts database set.
    /// </summary>
    public DbSet<AccountDb> Accounts { get; set; } = null!;

    /// <summary>
    /// Gets or sets users database set.
    /// </summary>
    public DbSet<UserDb> Users { get; set; } = null!;

    /// <summary>
    /// Gets or sets transactions database set.
    /// </summary>
    public DbSet<TransactionDb> Transactions { get; set; } = null!;

    /// <inheritdoc />
    /// <remarks>A applies custom model configuration.</remarks>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new UserDbEntityConfiguration());
        modelBuilder.ApplyConfiguration(new AccountDbEntityConfiguration());
        modelBuilder.ApplyConfiguration(new TransactionDbEntityConfiguration());
    }
}
