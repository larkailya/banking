using System.Linq.Expressions;
using Bank.Db.Abstractions.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Bank.Db.Sql.Repositories;

/// <summary>
/// Represents generic repository pattern.
/// </summary>
/// <typeparam name="TEntity">Specifies entity type.</typeparam>
public sealed class Repository<TEntity> : IRepository<TEntity> where TEntity : class
{
    private readonly BankDbContext _bankAccountingDbContext;

    /// <summary>
    /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
    /// </summary>
    /// <param name="bankAccountingDbContext">Provides database context.</param>
    public Repository(BankDbContext bankAccountingDbContext)
    {
        _bankAccountingDbContext = bankAccountingDbContext;
    }

    public async Task CreateAsync(TEntity entity, CancellationToken cancellationToken)
    {
        await _bankAccountingDbContext.Set<TEntity>()
            .AddAsync(entity, cancellationToken);
    }

    public Task UpdateAsync(int id, TEntity entity, CancellationToken cancellationToken)
    {
        _bankAccountingDbContext.Set<TEntity>()
            .Update(entity);
        
        return Task.CompletedTask;
    }

    public async Task<TEntity?> GetAsync(int id, CancellationToken cancellationToken)
    {
        return await _bankAccountingDbContext.Set<TEntity>()
            .FindAsync(new object?[] { id }, cancellationToken: cancellationToken);
    }

    // Pagination should applied but I simplify it
    public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken)
    {
        return await _bankAccountingDbContext.Set<TEntity>()
            .Where(filter)
            .ToListAsync(cancellationToken);
    }
}