using Bank.Db.Sql.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bank.Db.Sql.EntityConfigurations;

internal sealed class UserDbEntityConfiguration : IEntityTypeConfiguration<UserDb>
{
    public void Configure(EntityTypeBuilder<UserDb> builder)
    {
        builder.HasKey(user => user.Id);

        builder.Property(user => user.FirstName).IsRequired();
        
        builder.Property(user => user.LastName).IsRequired();

        builder.Property(user => user.ConcurrencyToken)
            .IsConcurrencyToken()
            .ValueGeneratedOnAddOrUpdate();

        builder
            .HasMany(user => user.Accounts)
            .WithOne(account => account.User)
            .HasForeignKey(user => user.UserId);
    }
}