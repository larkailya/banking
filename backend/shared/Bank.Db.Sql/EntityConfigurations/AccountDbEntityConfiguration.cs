using Bank.Db.Sql.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bank.Db.Sql.EntityConfigurations;

internal sealed class AccountDbEntityConfiguration : IEntityTypeConfiguration<AccountDb>
{
    public void Configure(EntityTypeBuilder<AccountDb> builder)
    {
        builder.HasKey(account => account.Id);

        builder.Property(account => account.ConcurrencyToken)
           .IsConcurrencyToken()
           .ValueGeneratedOnAddOrUpdate();
            
        builder
            .HasOne(account => account.User)
            .WithMany(user => user.Accounts)
            .HasForeignKey(account => account.UserId)
            .IsRequired();
    }
}