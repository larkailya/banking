using Bank.Db.Sql.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bank.Db.Sql.EntityConfigurations;

internal sealed class TransactionDbEntityConfiguration : IEntityTypeConfiguration<TransactionDb>
{
    public void Configure(EntityTypeBuilder<TransactionDb> builder)
    {
        builder.HasKey(transaction => transaction.Id);

        builder
            .HasOne(user => user.Account)
            .WithMany(account => account.Transactions)
            .HasForeignKey(user => user.AccountId)
            .IsRequired();
    }
}