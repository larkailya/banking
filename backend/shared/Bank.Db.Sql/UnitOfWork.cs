using Bank.Db.Abstractions.Interfaces;

namespace Bank.Db.Sql;

public sealed class UnitOfWork : IUnitOfWork
{
    private readonly BankDbContext _bankAccountingDbContext;

    public UnitOfWork(BankDbContext bankAccountingDbContext)
    {
        _bankAccountingDbContext = bankAccountingDbContext;
    }

    public async Task CommitAsync(CancellationToken cancellationToken)
    {
        await _bankAccountingDbContext.SaveChangesAsync(cancellationToken);
    }
}