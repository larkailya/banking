# Bank

## Run it

1. Go to infra folder
2. Run `docker-compose -f ./docker.compose.yml up -d `
    Hope it works. I have MacOS, it works on MacOS system. I am sorry I do not have windows to check everything.
3. Navigate to any of two API projects.
4. Run `dotnet ef database update`
5. Open postman folder and import collection to Postman
6. Run UI from bank-ui folder `ng serve`
